#ifndef BACKGROUNDAPPLIER_H
#define BACKGROUNDAPPLIER_H

#include "ColorStop.h"
#include <QWidget>

namespace Ui {
class BackgroundApplier;
}

struct Preset
{
    std::string name;
    QGradient::Type type;
    QGradientStops stops;
    unsigned int angle;
};

class BackgroundApplier : public QWidget
{
    Q_OBJECT

public:
    explicit BackgroundApplier(QWidget *parent = nullptr);
    ~BackgroundApplier();

    void SetSize(int,int);

    QImage GetImage();

signals:
    void BackgroundApplied(QImage) const;

private slots:
    void on_gradient_type_currentIndexChanged(int);
    void on_spread_comboBox_currentIndexChanged(int);
    void on_rotate_valueChanged(int);
    void on_add_color_stop_button_clicked();
    void on_choose_image_button_clicked();
    void on_presets_comboBox_currentIndexChanged(int);

private:
    Ui::BackgroundApplier *ui;
    int w;
    int h;

    QImage image;

    std::vector<ColorStop*> color_stops;

    void ApplyCustom();
    void OpenFileDialog();
    void AddColorStop(ColorStop*);

    QBrush ApplyLinearGradient(QGradientStops, QGradient::Spread);
    QBrush ApplyRadialGradient(QGradientStops, QGradient::Spread);
    QBrush ApplyConicalGradient(QGradientStops, QGradient::Spread);

    QPointF EdgeOfView(int);

    const QStringList custom_names = {
        "Linear", "Radial", "Conical"
    };

    const QStringList spread_names = {
        "Pad", "Repeat", "Reflect"
    };

    const std::vector<QGradient::Spread> spreads = {
        QGradient::PadSpread,
        QGradient::RepeatSpread,
        QGradient::ReflectSpread
    };

    const std::vector<Preset> presets = {
        {
            "Dark red",
            QGradient::LinearGradient,
            QGradientStops({
                QGradientStop(0,Qt::black),
                QGradientStop(1,Qt::red),
            }),
            0
        },
        {
            "Dark blue",
            QGradient::LinearGradient,
            QGradientStops({
                QGradientStop(0,Qt::black),
                QGradientStop(1,Qt::blue),
            }),
            0
        },
        {
            "Dark green",
            QGradient::LinearGradient,
            QGradientStops({
                QGradientStop(0,Qt::black),
                QGradientStop(1,Qt::green),
            }),
            0
        },
        {
            "Dark yellow",
            QGradient::LinearGradient,
            QGradientStops({
                QGradientStop(0,Qt::black),
                QGradientStop(1,Qt::yellow),
            }),
            0
        }
    };

};

#endif // BACKGROUNDAPPLIER_H
